import * as actions from '../actions';
import moment from 'moment';

const initialState = {
  loading: false,
  weatherId: null,
  name: '',
  temperature: '',
  weather_state_name: '',
  latitude: null,
  longitude: null,
  data: {},
  tempArray: [],
  timeArray: []
};

const toF = c => (c * 9) / 5 + 32;

const startLoading = (state, action) => {
  return { ...state, loading: true };
};

const weatherDataRecevied = (state, action) => {
  const { data } = action;
  if (!data['consolidated_weather']) return state;
  const drone = data.consolidated_weather[0];
  const { weather_state_name, the_temp } = drone;
  const { latt_long, title: name } = data;
  const [latitude, longitude] = latt_long.split(',');
  const getTime = moment(data.time).format('hh:mm:ss');

  const newTempArray = state.tempArray;
  if (newTempArray.length > 5) {
    newTempArray.shift();
  }
  newTempArray.push(the_temp);

  const newTimeArray = state.timeArray;
  if (newTimeArray.length > 5) {
    newTimeArray.shift();
  }
  newTimeArray.push(getTime);
  return {
    ...state,
    loading: false,
    latitude,
    longitude,
    temperatureinCelsius: the_temp,
    temperatureinFahrenheit: toF(the_temp),
    weather_state_name,
    name,
    data: action.data,
    tempArray: newTempArray,
    timeArray: newTimeArray
  };
};

const handlers = {
  [actions.FETCH_WEATHER]: startLoading,
  [actions.WEATHER_DATA_RECEIVED]: weatherDataRecevied
};

export default (state = initialState, action) => {
  const handler = handlers[action.type];
  if (typeof handler === 'undefined') return state;
  return handler(state, action);
};
