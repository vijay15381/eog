import { takeEvery, call, put, cancel, all } from 'redux-saga/effects';
import delay from '@redux-saga/delay-p';
import API from '../api';
import * as actions from '../actions';

function* watchWeatherIdReceived(action) {
  const { id } = action;

  while (true) {
    const { error, data } = yield call(API.findWeatherbyId, id);
    yield delay(4000);
    if (error) {
      yield put({ type: actions.API_ERROR, code: error.code });
      yield cancel();
      return;
    }
    yield put({ type: actions.WEATHER_DATA_RECEIVED, data });
  }
}

function* watchAppDroneLoad() {
  yield all([takeEvery(actions.WEATHER_ID_RECEIVED, watchWeatherIdReceived)]);
}

export default [watchAppDroneLoad];
