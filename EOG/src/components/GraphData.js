import React from 'react';
import { connect } from 'react-redux';
import { Line } from 'react-chartjs-2';

class GraphData extends React.Component {
  render() {
    // console.log('temp', this.props.tempArray);
    // console.log('time:', this.props.timeArray);

    const labelsArray = this.props.timeArray;
    const dataArray = this.props.tempArray;
    const data = {
      labels: [...labelsArray],
      // labels: ['21', '22', '24', '26', '28', '30', '32'],
      datasets: [
        {
          label: 'Temperature chart',
          fill: false,
          lineTension: 0.1,
          backgroundColor: 'rgba(75,192,192,0.4)',
          borderColor: 'rgba(75,192,192,1)',
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',
          pointBorderColor: 'rgba(75,192,192,1)',
          pointBackgroundColor: '#fff',
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: 'rgba(75,192,192,1)',
          pointHoverBorderColor: 'rgba(220,220,220,1)',
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          // data: [20,12,8,10,18, 20, 15]
          data: [...dataArray]
        }
      ]
    };
    return (
      <div style={{ width: 500, height: 500, padding: 25 }}>
        <Line data={data} />
      </div>
    );
  }
}

const mapState = (state, ownProps) => {
  const { data, temperatureinCelsius, timeArray, tempArray } = state.drone;
  return {
    data,
    temperatureinCelsius,
    timeArray,
    tempArray
  };
};

// const mapDispatch = dispatch => ({
//   onLoad: () =>
//     dispatch({
//       type: actions.FETCH_WEATHER,
//       longitude: -95.3698,
//       latitude: 29.7604
//     })
// });

export default connect(
  mapState,
  null
)(GraphData);
