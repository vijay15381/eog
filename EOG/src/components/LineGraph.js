import React from 'react';
import { GraphData } from './GraphData';
import { connect } from 'react-redux';
import * as actions from '../store/actions';
import LinearProgress from '@material-ui/core/LinearProgress';

class LineGraph extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  componentDidMount() {
    this.props.onLoad();
  }

  render() {
    const {
      loading,
      name,
      weather_state_name,
      temperatureinFahrenheit,
      temperatureinCelsius,
      longitude,
      latitude
    } = this.props;
    if (loading) return <LinearProgress />;
    return (
      <div>
        <GraphData data={this.props.onLoad} />
      </div>
    );
  }
}

const mapState = (state, ownProps) => {
  const {
    loading,
    name,
    weather_state_name,
    temperatureinFahrenheit,
    temperatureinCelsius,
    longitude,
    latitude
  } = state.weather;
  return {
    loading,
    name,
    weather_state_name,
    temperatureinFahrenheit,
    temperatureinCelsius
  };
};

const mapDispatch = dispatch => ({
  onLoad: () =>
    dispatch({
      type: actions.FETCH_WEATHER
    })
});

export default connect(
  mapState,
  mapDispatch
)(LineGraph);
