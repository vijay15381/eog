import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../store/actions';
import LinearProgress from '@material-ui/core/LinearProgress';
import ChipRaw from '@material-ui/core/Chip';
import { withStyles } from '@material-ui/core/styles';

const cardStyles = theme => ({
  root: {
    background: theme.palette.secondary.main
  },
  label: {
    color: theme.palette.primary.main
  }
});
const Chip = withStyles(cardStyles)(ChipRaw);

class Drone extends Component {
  componentDidMount() {
    this.props.onLoad();
  }
  render() {
    const { loading, temperatureinCelsius, longitude, latitude } = this.props;
    if (loading) return <LinearProgress />;
    return (
      <div style={{ textAlign: 'center' }}>
        <h2>Data Visualisation</h2>
        <div>
          <Chip label={`Temperature: ${temperatureinCelsius}°`} />
          <Chip label={`Longitude: ${longitude}`} />
          <Chip label={`Latitude: ${latitude}`} />
        </div>
      </div>
    );
  }
}

const mapState = (state, ownProps) => {
  const { loading, temperatureinCelsius, longitude, latitude } = state.drone;
  return {
    loading,
    temperatureinCelsius,
    longitude,
    latitude
  };
};

const mapDispatch = dispatch => ({
  onLoad: () =>
    dispatch({
      type: actions.FETCH_WEATHER,
      longitude: -95.3698,
      latitude: 29.7604
    })
});

export default connect(
  mapState,
  mapDispatch
)(Drone);
