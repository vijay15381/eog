import React from 'react';
import createStore from './store';
import { Provider } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import 'react-toastify/dist/ReactToastify.css';
import Header from './components/Header';
import Wrapper from './components/Wrapper';
import NowWhat from './components/NowWhat';
// import findLocationByLatLng from './store/api/findLocationByLatLng';
// import findWeatherbyId from './store/api/findWeatherById';
// import watchAppDroneLoad from './store/sagas/Drone';
// import watchAppLoad from './store/sagas/Weather';
import GraphData from './components/GraphData';
import Drone from './components/Drone';
const store = createStore();
const theme = createMuiTheme({
  typography: {
    useNextVariants: true
  },
  palette: {
    primary: {
      main: 'rgb(39,49,66)'
    },
    secondary: {
      main: 'rgb(197,208,222)'
    },
    background: {
      main: 'rgb(226,231,238)'
    }
  }
});

// const latitude = 26.949418334243543;
// const longitude = -91.23515201958546;
// const locationId = 2424766;

class App extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    // const resp= findLocationByLatLng(latitude, longitude);
    // const houstonLat = "29.760450";
    // const houstonLong = "-95.369781";
    // console.log("resp", resp);
    // const idResp = findWeatherbyId(locationId);
    // console.log("IDresp", idResp);
    // const { dispatch } = this.props;
    // dispatch({type:'COMMAND/FETCH_WEATHER_FOR_LAT_LNG', payload:{latitude, longitude}
  }
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <Provider store={store}>
          <Wrapper>
            <Header />
            <div>
              <Drone />
            </div>
            <div>
              <GraphData />
            </div>
            <NowWhat />
            <ToastContainer />
          </Wrapper>
        </Provider>
      </MuiThemeProvider>
    );
  }
}

export default App;
